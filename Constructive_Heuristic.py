#    factlog-steel-prod-optimization (c) by the University of Piraues, Greece.
#
#    factlog-steel-prod-optimization is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

import json
from itertools import combinations
from itertools import permutations
import numpy as np
import aux_functions as aux  

"""Load all the required data. Should we create a function for this step??"""
def Heuristic_model(data):

    N_Tasks = int(data['Tasks'])
    N_Machines = int(data['Machines'])
    N_Stages = int(data['Stages'])
    lambda_l = float(data['lambda'])
    
    Tasks = tuple(range(N_Tasks))
    Machines = tuple(range(N_Machines))
    Stages = tuple(range(N_Stages))
    
    MachinesPerStage = tuple(int(i) for i in data['MachinesPerStage'].split(","))
    
    Release= tuple(int(i) for i in data['ReleaseTimes'].split(","))
    Due= tuple(float(i) for i in data['DueTimes'].split(","))
    SetUp = tuple(float(i) for i in data['SetUpTimes'].split(","))
    MachineStatus = tuple(int(i) for i in data['MachineStatus'].split(","))
    Type = tuple(int(i) for i in data['Type'].split(","))
    
    Processing = []
    for i in data['ProcessingTimes']:
       Processing.append(tuple(float(j) for j in i.split(",")))
    Processing = tuple(Processing)
    
    ForbiddenPaths = []
    for i in data['ForbiddenPaths']:
        ForbiddenPaths.append(tuple(int(j) for j in i.split(",")))
    ForbiddenPaths = tuple(ForbiddenPaths)
    
    # Generate a tuple with the cumulative number of machines on each stage starting from 0
    CumMachinesPerStage = tuple([sum(MachinesPerStage[0:x:1]) for x in range(0, N_Stages+1)])
    
    
    ForbiddenTasksToMachines = []     # produce all the forbidden task assignements to machines
    #=================== IT ONLY WORKS FOR 3 STAGES ====================================
    for i in data['ForbiddenTasksToMachines']:  # take any potentials forbidden assignments that the user have given
        ForbiddenTasksToMachines.append(tuple(int(j) for j in i.split(",")))
    
    No_working_machine_idx = [i for i in range(len(MachineStatus)) if MachineStatus[i] == 0]
    
    for m in No_working_machine_idx:
        if m in range(CumMachinesPerStage[0],CumMachinesPerStage[1]):
            for t in Tasks:
                if Type[t] == 0:
                    ForbiddenTasksToMachines.append((t,m))
        elif m in range(CumMachinesPerStage[1],CumMachinesPerStage[2]):
            for t in Tasks:
                if Type[t] == 1:
                    ForbiddenTasksToMachines.append((t,m))
        else:
            for t in Tasks:
                if Type[t] == 2:
                    ForbiddenTasksToMachines.append((t,m))
    
    for i in Tasks:                   
        if Type[i] == 0:    # coil jobs
            for m in range(CumMachinesPerStage[1],CumMachinesPerStage[3]):  # exclude all the machines at stage 1 & 2
                ForbiddenTasksToMachines.append((i,m))
        elif Type[i] == 1:  # bar jobs
            for m in range(CumMachinesPerStage[1]):     # exclude all the machines at stage 0
                ForbiddenTasksToMachines.append((i,m))
            for m in range(CumMachinesPerStage[2],CumMachinesPerStage[3]):  # exclude all the machines at stage 2
                ForbiddenTasksToMachines.append((i,m))
        else:               # bending jobs
            for m in range(CumMachinesPerStage[1]):     # exclude all the machines at stage 0
                ForbiddenTasksToMachines.append((i,m))
    
    """Create the arrays that will help us to construct our solution, namely the array with the
    assignments X_{im} which is 1 iff job i is assigned to machine m, L_{i} the lateness of job i,
    T_{i} which is 1 iff L_{i} > 0, CT_{m} is the completion time for machine m and which is able to 
    process the next job, FT_{is} is the finishing time for job i in stage s"""
    
    X = np.zeros(shape=(N_Tasks,N_Machines), dtype = int)
    
    L = np.zeros(shape=(N_Tasks), dtype = float)
    
    T = np.zeros(shape=(N_Tasks), dtype = int)
    
    CT = np.zeros(shape = (N_Machines))
    
    FT = np.zeros(shape=(N_Tasks,N_Stages))
    
    def job_priority_rule(lambda_l,Tasks):
        """
        The priority rule is based on a ratio for every job where the enumerator is the worst assignment among 
        all valid divided by its Due date. After sorting the list in descheding order we start to assign the
        jobs on the machines. A large ratio means that the due date for this job is very tight or the worst 
        assignent is time consuming or both is hold.
        """
        priority_dict = {}
        if lambda_l >= 0:
            for i in Tasks:
                if Type[i] == 0:
                    priority_dict[i] = \
                    max([Processing[m][i] + SetUp[m] for m in range(CumMachinesPerStage[0],CumMachinesPerStage[1])
                    if (i,m) not in ForbiddenTasksToMachines]) / Due[i]
                
                elif Type[i] == 1:
                    priority_dict[i] = \
                    max([Processing[m][i] + SetUp[m] for m in range(CumMachinesPerStage[1],CumMachinesPerStage[2])
                    if (i,m) not in ForbiddenTasksToMachines]) / Due[i]
                
                else:
                    priority_dict[i] = \
                    ( max([Processing[m][i] + SetUp[m] for m in range(CumMachinesPerStage[1],CumMachinesPerStage[2])
                    if (i,m) not in ForbiddenTasksToMachines]) + 
                    max([Processing[m][i] + SetUp[m] for m in range(CumMachinesPerStage[2],CumMachinesPerStage[3])
                    if (i,m) not in ForbiddenTasksToMachines]) ) / Due[i]
            
        # sort the dictionary over the values in descending order and return the keys
        priority_dict_sorted = {k:v for k,v in sorted(priority_dict.items(), key=lambda item:item[1],reverse=True)}
        return(priority_dict_sorted.keys())    
    
    
    
    def assignment_rule(i,s):
        """
        This rule assigns the jobs on the machines that is compatible with the job's specs. If we have coil/cutting
        related jobs then the assignment rule take into consideration the max(Completion_Time for machine m, release
        time for job i) adding the processing and setup time related with machine m. With the maximum (CT, release)
        we ensure that the machine doen't process another job and the job's release date has passed. After the
        examination of all valid jobs' assignments on the machines we take the minimum Finishing Time for the job.
        So we are looking for: arg min_{m} [max(CT[m], release[i]) + processing + setup ]
        
        On the other hand if we have a bending job then the idea is the same but instead of having max(CT, release)
        we have max(Completion time of machine m, Finishing time of job i in the Previous stage)
        """
        assignment = {}
        if s != 2:  # coil / cutting jobs
            for m in range(CumMachinesPerStage[s],CumMachinesPerStage[s+1]):
                if (i,m) not in ForbiddenTasksToMachines:
                    assignment[(i,m)] = max(CT[m],Release[i]) + Processing[m][i] + SetUp[m]
            m = aux.getKey(assignment,min(assignment.values()))[0][1]   # get the machine
            F_T = assignment[i,m]
        
        else:       # bending jobs
            for m in range(CumMachinesPerStage[s],CumMachinesPerStage[s+1]):
                if (i,m) not in ForbiddenTasksToMachines:
                    assignment[(i,m)] = max(CT[m],FT[i,s-1]) + Processing[m][i] + SetUp[m]
            m = aux.getKey(assignment,min(assignment.values()))[0][1]
            F_T = assignment[i,m]
        
        return(m,F_T)
        
    
    for i in job_priority_rule(lambda_l,Tasks):
        if Type[i] == 0:
            s = 0                                       # we are in the first stage
            (m,FT[i,s]) = assignment_rule(i,s)          # get the machine that the job is assigned and the job's completion time
            CT[m] = FT[i,s]                             # job's completion time = machine completion time
            X[i,m] = 1                                  # assign the job on that machine
            L[i] = max(FT[i,s] - Due[i], 0)             # lateness
            if L[i] > 0:
                T[i] = 1                                # tardiness
            
        elif Type[i] == 1:                              # we are in the second stage
            s = 1
            (m,FT[i,s]) = assignment_rule(i,s)          # get the machine that the job is assigned and the job's completion time
            CT[m] = FT[i,s]                             # job's completion time = machine completion time
            X[i,m] = 1  
            L[i] = max(FT[i,s] - Due[i], 0)             # lateness
            if L[i] > 0:
                T[i] = 1                                # tardiness
                
        else:                                           # we are in the third stage
            s = 1                                       # make an assignement to cutting machines
            (m,FT[i,s]) = assignment_rule(i,s)          # get the machine that the job is assigned and the job's completion time
            CT[m] = FT[i,s]                             # job's completion time = machine completion time
            X[i,m] = 1 
            
            s = 2                                       # make an assignemnt to bending machines
            (m,FT[i,s]) = assignment_rule(i,s)          # get the machine that the job is assigned and the job's completion time
            CT[m] = FT[i,s]                             # job's completion time = machine completion time
            X[i,m] = 1 
            L[i] = max(FT[i,s] - Due[i], 0)             # lateness
            if L[i] > 0:
                T[i] = 1                                # tardiness
                
    # The makespan is the bigger of all completion times
    C_max = FT.max()
    
    """
    For the post-process procedure we need the sets and the solution arrays to be manipulated 
    using also the mapping objects from Input_Preprocess to deliver the final output file.
    """
    Heuristic_output = Tasks, Machines, Stages, CumMachinesPerStage, FT,X,C_max,L,T
    return(Heuristic_output)   

















