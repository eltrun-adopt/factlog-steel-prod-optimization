#    factlog-steel-prod-optimization (c) by the University of Piraues, Greece.
#
#    factlog-steel-prod-optimization is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

FROM registry.gitlab.com/eltrun-adopt/cplex as cplex
ENV RB_HOST $RB_HOST
ENV RB_PORT $RB_PORT
ENV RB_USER $RB_USER
ENV RB_PASS $RB_PASS

FROM python:3.7-slim
COPY --from=cplex /opt/ /opt
ENV PATH="/opt/ibm/ILOG/CPLEX_Studio201/cplex/bin/x86-64_linux:${PATH}"

WORKDIR /brc
COPY ./ ./
RUN pip install -r requirements.txt
ENTRYPOINT python MIP_model.py $RB_HOST $RB_PORT $RB_USER $RB_PASS