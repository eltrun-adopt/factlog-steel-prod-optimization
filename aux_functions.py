#    factlog-steel-prod-optimization (c) by the University of Piraues, Greece.
#
#    factlog-steel-prod-optimization is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

import json
import pandas as pd
import numpy as np
import itertools


def getKey(dct,value):
        """
        Function to retrieve the key of a dictionary given the value.
        
        check if the dictionary consist of pairs like key:int/float/bool so we search via the first
        statement (dct[key] == value).
        
        Otherwise if the dictionary is key:str (e.g. 0: '5201210118005935') we search using the second
        statement (value in dct[key])
        """
        #if (type(value) == bool or type(value) == int or type(value) == float) :
        if type(value) != str :
            return ([key for key in dct if (dct[key] == value)])
        else:
            return ([key for key in dct if (value in dct[key])])
        

def time_to_min(t):
    """ This function convert the time str into minutes as a number
    e.g. "00:01:20" --> 1.33'
    """
    h, m, s = map(int, t.split(':'))
    return (h * 60 + m + s/60)
