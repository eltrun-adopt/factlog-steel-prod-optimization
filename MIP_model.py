#    factlog-steel-prod-optimization (c) by the University of Piraues, Greece.
#
#    factlog-steel-prod-optimization is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

import pyomo.environ as pyo
import json
from itertools import combinations
from itertools import permutations
import pika
import time
import sys
from datetime import datetime
from Input_preprocess import *
from Output_postprocess import *
import aux_functions as aux
from Constructive_Heuristic import *

def MIP_test_model(data):
    
    model = pyo.ConcreteModel()
    
    N_Tasks = int(data['Tasks'])
    N_Machines = int(data['Machines'])
    N_Stages = int(data['Stages'])
    lambda_l = float(data['lambda'])
    
    Tasks = tuple(range(N_Tasks))
    Machines = tuple(range(N_Machines))
    Stages = tuple(range(N_Stages))
    
    MachinesPerStage = tuple(int(i) for i in data['MachinesPerStage'].split(","))
    
    Release= tuple(int(i) for i in data['ReleaseTimes'].split(","))
    Due= tuple(float(i) for i in data['DueTimes'].split(","))
    SetUp = tuple(float(i) for i in data['SetUpTimes'].split(","))
    MachineStatus = tuple(int(i) for i in data['MachineStatus'].split(","))
    Type = tuple(int(i) for i in data['Type'].split(","))
    
    Processing = []
    for i in data['ProcessingTimes']:
       Processing.append(tuple(float(j) for j in i.split(",")))
    Processing = tuple(Processing)
    
    ForbiddenPaths = []
    for i in data['ForbiddenPaths']:
        ForbiddenPaths.append(tuple(int(j) for j in i.split(",")))
    ForbiddenPaths = tuple(ForbiddenPaths)
    
    PerTasks = tuple(permutations(Tasks,2))
    ComTasks = tuple(combinations(Tasks,2))
    
    #Time Upper bound 
    U = sum(max(Processing[m][t] for m in Machines) for t in Tasks)
    
    # Generate a tuple with the cumulative number of machines on each stage starting from 0
    CumMachinesPerStage = tuple([sum(MachinesPerStage[0:x:1]) for x in range(0, N_Stages+1)])
    
    ForbiddenTasksToMachines = []     # produce all the forbidden task assignements to machines
    #=================== IT ONLY WORKS FOR 3 STAGES ====================================
    for i in data['ForbiddenTasksToMachines']:  # take any potentials forbidden assignments that the user have given
        ForbiddenTasksToMachines.append(tuple(int(j) for j in i.split(",")))
    
    for i in Tasks:                   
        if Type[i] == 0:    # coil jobs
            for m in range(CumMachinesPerStage[1],CumMachinesPerStage[3]):  # exclude all the machines at stage 1 & 2
                ForbiddenTasksToMachines.append((i,m))
        elif Type[i] == 1:  # bar jobs
            for m in range(CumMachinesPerStage[1]):     # exclude all the machines at stage 0
                ForbiddenTasksToMachines.append((i,m))
            for m in range(CumMachinesPerStage[2],CumMachinesPerStage[3]):  # exclude all the machines at stage 2
                ForbiddenTasksToMachines.append((i,m))
        else:               # bending jobs
            for m in range(CumMachinesPerStage[1]):     # exclude all the machines at stage 0
                ForbiddenTasksToMachines.append((i,m))
                
    #Time stamp variables, I created a LB for these decisions variables
    def bnd_rule_FT(model,i,j):
        LB_list = []
        for m in range(CumMachinesPerStage[j],CumMachinesPerStage[j+1]):
            if (i,m) not in ForbiddenTasksToMachines:
                LB_list.append(Processing[m][i] + SetUp[m] + Release[i])
        LB = min(LB_list, default=0)
        return(LB,None)    
    model.FT= pyo.Var(Tasks, Stages,initialize=0, within=pyo.NonNegativeReals, bounds = bnd_rule_FT)
    
    #Makespan variable
    model.C_max = pyo.Var(initialize=0,within=pyo.NonNegativeReals)
    
    #Assignment Variables
    def bnd_rule_Y(model,i,j):
        if((j,i) in ForbiddenTasksToMachines):
            return(0,0)     # so Y_mi = 0, iff this assignment is inside forbidden assignment list
        else:
            return(None,MachineStatus[i])
    model.Y = pyo.Var(Machines,Tasks,initialize=0, within=pyo.Binary,
                     bounds=bnd_rule_Y)
    #Precedence Variables 
    model.X = pyo.Var(ComTasks, Stages ,initialize=0, within=pyo.Binary)  
     
    #Lateness and Tardiness declaration
    model.L = pyo.Var(Tasks, initialize=0, within=pyo.NonNegativeReals)     # lateness variable to calculate the difference between completion time and due date
    
    model.T = pyo.Var(Tasks, initialize=0, within=pyo.Binary)               # 1 iff a job is tardy
    
    #Objective Function       
    model.obj = pyo.Objective(expr = lambda_l * model.C_max + (1-lambda_l) * 
                              sum(model.L[i] for i in Tasks))  
    
    #Cons: Makespan should be >= completion time for every task
    def c_max_rule(model,i,s):
        return(model.C_max >= model.FT[i,s])
    model.Makespan = pyo.Constraint(Tasks,Stages,rule=c_max_rule)
    
    #Cons: Assignment Constraints per Stage
    model.AssignCons = pyo.ConstraintList()
    
    #Cons: Activation Constraints 
    model.ActivationCons = pyo.ConstraintList()  
    
    #BndCons for Finish Time vars
    model.FinishTimeBndCons = pyo.ConstraintList()
    
    for s in Stages:
        for i in Tasks:
            for m in range(CumMachinesPerStage[s],CumMachinesPerStage[s+1]):
                if (i,m) not in ForbiddenTasksToMachines:
                    model.AssignCons.add(sum(model.Y[m,i] for m in range(CumMachinesPerStage[s],CumMachinesPerStage[s+1])) == 1)
                   
                    model.FinishTimeBndCons.add(model.FT[i,s] >=
                            sum((Release[i]+SetUp[m]+Processing[m][i])*model.Y[m,i] 
                            for m in range(CumMachinesPerStage[s],CumMachinesPerStage[s+1])))
            
                
    for s in Stages[:-1]:
        for i in Tasks:
            model.FinishTimeBndCons.add(model.FT[i,s] <= model.FT[i,s+1]-sum(model.Y[m,i]*
                (Processing[m][i]+SetUp[m]) for m in range(CumMachinesPerStage[s+1],
                CumMachinesPerStage[s+2])))
    
    
    model.PrecedenceCons = pyo.ConstraintList()        
    for t in ComTasks:
        for s in Stages:
            for m in range(CumMachinesPerStage[s],CumMachinesPerStage[s+1]):
                model.PrecedenceCons.add(model.FT[t[1],s] >= model.FT[t[0],s]+Processing[m][t[1]] 
                + SetUp[m] - U*(3 - model.X[t,s]- model.Y[m,t[0]] - model.Y[m,t[1]]))
                
                model.PrecedenceCons.add(model.FT[t[0],s] >= model.FT[t[1],s]+Processing[m][t[0]] 
                + SetUp[m] - U*(2 + model.X[t,s]- model.Y[m,t[0]] - model.Y[m,t[1]]))
    
    #Constraints for forbidden paths between machines in differet stages
    model.ForbiddenPathCons = pyo.ConstraintList()            
    for f in ForbiddenPaths:
        for t in Tasks:
            model.ForbiddenPathCons.add(model.Y[f[0],t] + model.Y[f[1],t] <= 1)
    
    #Constraints for tardy jobs
    model.LatenessCons = pyo.ConstraintList()
    for s in Stages:
        for i in Tasks:
            model.LatenessCons.add(model.L[i] >= model.FT[i,s] - Due[i])    # lateness calculation
    
    model.TardinessCons = pyo.ConstraintList()
    for i in Tasks:
        model.TardinessCons.add(model.L[i] <= U*model.T[i])
        model.TardinessCons.add(model.T[i] <= U*model.L[i])
            
    #Derivation of Upper and lower bounds for processing times at each stage
    Processing_Bound=[]
    for s in Stages:
        if s>0:
            Processing_LB= \
                min(Release[t]+
                     sum(min([Processing[m][t]+SetUp[m] for m in range(CumMachinesPerStage[j],CumMachinesPerStage[j+1]) # min over all machines in earlier stages
                     if (t,m) not in ForbiddenTasksToMachines],default=0) 
                     for j in range(s))    #for all earlier stages
                     for t in Tasks )
        else:
            Processing_LB= \
                min([Release[t] for t in Tasks])
        
        if s<N_Stages-1:
            Processing_UB = \
                    max(Due[t]-
                         sum(min([Processing[m][t]+SetUp[m] for m in range(CumMachinesPerStage[j],CumMachinesPerStage[j+1]) # min over all machines in later stages
                         if (t,m) not in ForbiddenTasksToMachines],default=0) 
                         for j in range(s+1,N_Stages) )     # for all later stages
                         for t in Tasks)
        else:
            Processing_UB = \
                    max([Due[t] for t in Tasks])
                    
        # to avoid infeasibility due to negative (or very tight) Processing_Bound 
        if (Processing_UB - Processing_LB) >= min(sum(Processing[m][t]+SetUp[m] for t in Tasks) # min over all machines in earlier stages
                                  for m in range(CumMachinesPerStage[s],CumMachinesPerStage[s+1])   ):
            Processing_Bound.append(Processing_UB - Processing_LB)
        else:       # make the Processing_Bound redundant
            Processing_Bound.append(U)
        
    #Tightening Constraints       
    model.TightCons = pyo.ConstraintList() 
    for s in Stages:
        for m in range(CumMachinesPerStage[s],CumMachinesPerStage[s+1]):
            model.TightCons.add(sum((Processing[m][t]+SetUp[m])*model.Y[m,t] 
            for t in Tasks) <= Processing_Bound[s])
        
    
    solver = pyo.SolverFactory('cplex')
    solver.options['timelimit'] = 30  # run for 0.5 minutes 
    # status = solver.solve(model , tee = True, logfile = 'cplex.log')
    status = solver.solve(model)
    obj_val = pyo.value(model.obj)
    
    
    """
    For the post-process procedure we need the sets and the solution arrays to be manipulated 
    using also the mapping objects from Input_Preprocess to deliver the final output file.
    """
    MIP_output = Tasks, Machines, Stages, CumMachinesPerStage, model.FT,model.Y,model.C_max, model.L, model.T
    return(MIP_output)
       
    
host = sys.argv[1]
port = sys.argv[2]
username = sys.argv[3]
password = sys.argv[4]

credentials = pika.PlainCredentials(username, password)
params = pika.ConnectionParameters(host, port, '/', credentials, heartbeat=1860, blocked_connection_timeout=930)
connection = pika.BlockingConnection(params)
channel = connection.channel()

def callback(ch, method, properties, body):
    #print(" [x] Received %r" % body.decode())
    input_file = json.loads(body)
    uuid = input_file['uuid']
    data = input_file['data']
    try : 
        print("%s: Job with uuid: %s received" %(datetime.now().strftime("%d/%m/%Y %H:%M:%S"),uuid))
        print(body)
        preprocess_obj = Input_Preprocess(body)
        processed_data = preprocess_obj[0]
        input_data = json.loads(processed_data)
        MIP_solution = MIP_test_model(input_data)
        Heuristic_solution = Heuristic_model(input_data)
        
        MIP_output_data = Output_MIP_Postprocess(preprocess_obj[1],MIP_solution)
        Heuristic_output_data = Output_Heuristic_Postprocess(preprocess_obj[1],Heuristic_solution)
        
        MIP_makespan = MIP_output_data['ObjectiveValues']['Makespan']
        Heuristic_makespan = Heuristic_output_data['ObjectiveValues']['Makespan']
        output_data = {}
        if MIP_makespan > 0 :
            if MIP_makespan <= Heuristic_makespan:
                output_data = MIP_output_data
            else:
                output_data = Heuristic_output_data
        else:
            output_data = Heuristic_output_data
         
        objectives = output_data['ObjectiveValues']
        if objectives['Makespan']==0 and objectives['TotalLateness']==0 and objectives['TotalTardiness']==0:
            output_data = {"message" : "Invalid input data."}
            output = {"uuid" : uuid, "data" : output_data, "produced_at" : int(time.time() * 1000) }
        else:
            output = {"uuid" : uuid, "data" : output_data, "produced_at" : int(time.time() * 1000) }
        output = json.dumps(output)
        channel.basic_publish(exchange='opt-result', routing_key='multi-stage-flowshop', body=output)
        print("%s: Job with uuid: %s completed" %(datetime.now().strftime("%d/%m/%Y %H:%M:%S"),uuid))
    except Exception as e :
        print(str(e))
        error_message = {"message" : str(e)}
        error_responce = {"uuid": uuid , "data" : error_message, "produced_at" : int(time.time() * 1000)}
        error_responce = json.dumps(error_responce)
        channel.basic_publish(exchange='opt-result', routing_key='multi-stage-flowshop', body=error_responce)
        print("%s: Job with uuid: %s failed" %(datetime.now().strftime("%d/%m/%Y %H:%M:%S"),uuid))

channel.basic_consume(queue='multi-stage-flowshop_job',
                      auto_ack=True,
                      on_message_callback=callback)

channel.start_consuming()
