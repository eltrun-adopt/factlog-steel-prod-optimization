#    factlog-steel-prod-optimization (c) by the University of Piraues, Greece.
#
#    factlog-steel-prod-optimization is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

import pyomo.environ as pyo
import json
from itertools import combinations
from itertools import permutations
from Input_preprocess import *

"""
We have two different functions. One for the MIP post process and one for the Heuristic post-process. The 
reason lies in the fact of different data objects namely the assignment variables (model.Y) is a pyomo object 
creating in the MIP program however the corresponding assignment variables (X) for the Heuristic model
are np.arrays so there is a different way for processing each data type
"""

def Output_MIP_Postprocess(mapping_obj,solution):
    """
    First we need to define explicitly whatever is needed for the post-process. That's why it's easier
    for the developer to understand what's going on. Hence we'll define all the required set, decision
    arrays, mapping list etc
    """
    

    # Mapping
    machines_df = mapping_obj[0]
    order_id = mapping_obj[1]
    orders_jobs = mapping_obj[2]
    job_id = mapping_obj[3]
    ProcessingTimes = mapping_obj[4]
    Type = mapping_obj[5]
    
    # MIP solution output
    Tasks = solution[0]
    Machines = solution[1]
    Stages = solution[2]
    CumMachinesPerStage = solution[3]
    FT = solution[4]
    Y = solution[5]
    C_max = solution[6]
    L = solution[7]
    T = solution[8]
    
    output = {"OutputOrder":{}, "OuputJob":{} , "OutputMachine":{}, "ObjectiveValues":{}}
     
    
    for i in order_id:
        tardy_jobs = []
        for j in orders_jobs[i]:
            if T[getKey(job_id,j)[0]].value == 1:
                tardy_jobs.append(j)
        """
        using dict I save one reduntant layer (the index of the list)
        
        output['OutputOrder'][0]['C202013049SEQ-2000'] instead of
        output['OutputOrder']['C202013049SEQ-2000']
        """
        #output["OutputOrder"].append({i:tardy_jobs})
        output["OutputOrder"][str(i)] = tardy_jobs
     
    # OutputJob
    for t in Tasks:
        if Type[t] == 0:
            machines = []
            starting_time = []
            completion_time = []
            for m in range(CumMachinesPerStage[0],CumMachinesPerStage[1]):
                if round(Y[m,t].value) >= 1: # to prevent round error eg Y[8,9] = 0.9999999999888617
                    machines.append(machines_df.loc[m,'machineId'])
                    
                    # starting time
                    starting_time.append(abs(round(FT[t,0].value - ProcessingTimes[m][t] - 
                                         machines_df.loc[m,'setupTime_min'],2)))
                    # completion time
                    completion_time.append(round(FT[t,0].value,2))
                    
        elif Type[t] == 1:
            machines = []
            starting_time = []
            completion_time = []
            for m in range(CumMachinesPerStage[1],CumMachinesPerStage[2]):
                if round(Y[m,t].value) >= 1:
                    machines.append(machines_df.loc[m,'machineId'])
                    
                    # starting time
                    starting_time.append(abs(round(FT[t,1].value - ProcessingTimes[m][t] - 
                                         machines_df.loc[m,'setupTime_min'],2)))
                    # completion time
                    completion_time.append(round(FT[t,1].value,2))
                    
        else :
            machines = []
            starting_time = []
            completion_time = []
            for m in range(CumMachinesPerStage[1],CumMachinesPerStage[2]):
                if round(Y[m,t].value) >= 1:
                    machines.append(machines_df.loc[m,'machineId'])
                    
                    # starting time
                    starting_time.append(abs(round(FT[t,1].value - ProcessingTimes[m][t] - 
                                         machines_df.loc[m,'setupTime_min'],2)))
                    # completion time
                    completion_time.append(round(FT[t,1].value,2))
                    
            for m in range(CumMachinesPerStage[2],CumMachinesPerStage[3]):
                if round(Y[m,t].value) >= 1:
                    machines.append(machines_df.loc[m,'machineId'])
                    
                    # starting time
                    starting_time.append(abs(round(FT[t,2].value - ProcessingTimes[m][t] - 
                                         machines_df.loc[m,'setupTime_min'],2)))
                    # completion time
                    completion_time.append(round(FT[t,2].value,2))
            
        output["OuputJob"][job_id[t]] = {"parentID":getKey(orders_jobs,job_id[t])[0],"Machines":machines,
              'startTime': starting_time ,"completionTime": completion_time}
            
               
    #OutputMachine   
    for s in Stages:
        for m in range(CumMachinesPerStage[s],CumMachinesPerStage[s+1]):    # for m machines in each stage
            tasks = []
            for t in Tasks:
                if round(Y[m,t].value) >= 1:   # give those machines which have at least one job assigned
                    tasks.append(t)
            temp = {key: FT[key,s].value for key in tasks}    # create a dict with pairs task:finishing time like: {task:FT}
            temp_sorted = {k:v for k,v in sorted(temp.items(), key=lambda item:item[1])} # sort the dictionary over the values and return the keys
            if not tasks:
                next
            else:
                output["OutputMachine"][machines_df.loc[m,'machineId']] = {"jobID": [job_id[t] 
                    for t in temp_sorted.keys() ] }
                
                
    #ObjectiveValues
    output["ObjectiveValues"] = {"Makespan": round(C_max.value,2), "TotalTardiness": 
            int(sum(T[i].value for i in Tasks)), "TotalLateness" : sum(L[i].value for i in Tasks)}
    
    return(output)


def Output_Heuristic_Postprocess(mapping_obj,solution):
    """
    First we need to define explicitly whatever is needed for the post-process. That's why it's easier
    for the developer to understand what's going on. Hence we'll define all the required set, decision
    arrays, mapping list etc
    """
    

    # Mapping
    machines_df = mapping_obj[0]
    order_id = mapping_obj[1]
    orders_jobs = mapping_obj[2]
    job_id = mapping_obj[3]
    ProcessingTimes = mapping_obj[4]
    Type = mapping_obj[5]
    
    # Heuristic solution output
    Tasks = solution[0]
    Machines = solution[1]
    Stages = solution[2]
    CumMachinesPerStage = solution[3]
    FT = solution[4]
    X = solution[5]
    C_max = solution[6]
    L = solution[7]
    T = solution[8]
    
    output = {"OutputOrder":{}, "OuputJob":{} , "OutputMachine":{}, "ObjectiveValues":{}}
     
    
    for i in order_id:
        tardy_jobs = []
        for j in orders_jobs[i]:
            if T[getKey(job_id,j)[0]] == 1:
                tardy_jobs.append(j)
        """
        using dict I save one reduntant layer (the index of the list)
        
        output['OutputOrder'][0]['C202013049SEQ-2000'] instead of
        output['OutputOrder']['C202013049SEQ-2000']
        """
        #output["OutputOrder"].append({i:tardy_jobs})
        output["OutputOrder"][str(i)] = tardy_jobs
     
    # OutputJob
    for t in Tasks:
        if Type[t] == 0:
            machines = []
            starting_time = []
            completion_time = []
            for m in range(CumMachinesPerStage[0],CumMachinesPerStage[1]):
                if X[t,m] == 1:         
                    machines.append(machines_df.loc[m,'machineId'])
                    
                    # starting time
                    starting_time.append(abs(round(FT[t,0] - ProcessingTimes[m][t] - 
                                         machines_df.loc[m,'setupTime_min'],2)))
                    # completion time
                    completion_time.append(round(FT[t,0],2))
                    
        elif Type[t] == 1:
            machines = []
            starting_time = []
            completion_time = []
            for m in range(CumMachinesPerStage[1],CumMachinesPerStage[2]):
                if X[t,m] == 1:
                    machines.append(machines_df.loc[m,'machineId'])
                    
                    # starting time
                    starting_time.append(abs(round(FT[t,1] - ProcessingTimes[m][t] - 
                                         machines_df.loc[m,'setupTime_min'],2)))
                    # completion time
                    completion_time.append(round(FT[t,1],2))
                    
        else :
            machines = []
            starting_time = []
            completion_time = []
            for m in range(CumMachinesPerStage[1],CumMachinesPerStage[2]):
                if X[t,m] == 1:
                    machines.append(machines_df.loc[m,'machineId'])
                    
                    # starting time
                    starting_time.append(abs(round(FT[t,1] - ProcessingTimes[m][t] - 
                                         machines_df.loc[m,'setupTime_min'],2)))
                    # completion time
                    completion_time.append(round(FT[t,1],2))
                    
            for m in range(CumMachinesPerStage[2],CumMachinesPerStage[3]):
                if X[t,m] == 1:
                    machines.append(machines_df.loc[m,'machineId'])
                    
                    # starting time
                    starting_time.append(abs(round(FT[t,2] - ProcessingTimes[m][t] - 
                                         machines_df.loc[m,'setupTime_min'],2)))
                    # completion time
                    completion_time.append(round(FT[t,2],2))
            
        output["OuputJob"][job_id[t]] = {"parentID":getKey(orders_jobs,job_id[t])[0],"Machines":machines,
              'startTime': starting_time ,"completionTime": completion_time}
            
               
    #OutputMachine   
    for s in Stages:
        for m in range(CumMachinesPerStage[s],CumMachinesPerStage[s+1]):    # for m machines in each stage
            tasks = []
            for t in Tasks:
                if X[t,m] == 1:   # give those machines which have at least one job assigned
                    tasks.append(t)
            temp = {key: FT[key,s] for key in tasks}    # create a dict with pairs task:finishing time like: {task:FT}
            temp_sorted = {k:v for k,v in sorted(temp.items(), key=lambda item:item[1])} # sort the dictionary over the values and return the keys
            if not tasks:
                next
            else:
                output["OutputMachine"][machines_df.loc[m,'machineId']] = {"jobID": [job_id[t] 
                    for t in temp_sorted.keys() ] }
                
                
    #ObjectiveValues
    output["ObjectiveValues"] = {"Makespan": round(C_max,2), "TotalTardiness": 
            int(sum(T[i] for i in Tasks)), "TotalLateness" : sum(L[i] for i in Tasks)}
    
    return(output)



