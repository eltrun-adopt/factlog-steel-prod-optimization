# factlog: Steel Production Optimization


## Description
This code handles a multistage flowshop with parallel machines at each
stage for the steel industry. The goal of optimization is
to find the optimal production schedule in relation to the makespan or the number of tardy
jobs. This case was solved utilizing MILP for an extended flexible multistage flowshop
problem with machine dependent setup times. Preliminary experimentation showed that the
MIP model can handle instances of medium size quite easily and can provide production
policies that balance between the criterion of minimum makespan and tardy jobs.

## Authors
People who have contributed to this project are Kyriakos Bitsis and Dr. Konstantinos Kaparis.

## Acknowledgement
This research was funded by the H2020 FACTLOG project, which has received funding
from the European Union’s Horizon 2020 programme under grant agreement No. 869951.
<https://www.factlog.eu/>

## License
factlog-steel-prod-optimization (c) by the University of Piraeus, Greece.

factlog-steel-prod-optimization is licensed under a
Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.

You should have received a copy of the license along with this
work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.