#    factlog-steel-prod-optimization (c) by the University of Piraues, Greece.
#
#    factlog-steel-prod-optimization is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.


import json
import pandas as pd
import numpy as np
import itertools
from datetime import datetime
import random
from random import seed
from random import randint

def getKey(dct,value):
        """
        Function to retrieve the key of a dictionary given the value.
        
        check if the dictionary consist of pairs like key:int/float/bool so we search via the first
        statement (dct[key] == value).
        
        Otherwise if the dictionary is key:str (e.g. 0: '5201210118005935') we search using the second
        statement (value in dct[key])
        """
        if (type(value) == bool or type(value) == int or type(value) == float) :
            return [key for key in dct if (dct[key] == value)]
        else:
            return [key for key in dct if (value in dct[key])]

# function to convert the time str into minutes
def time_to_min(t):
    h, m, s = map(int, t.split(':'))
    return (h * 60 + m + s/60)
        
def Input_Preprocess(raw_data):
    ###################################### LOAD THE DATA  ###################################
    
    data_df = pd.read_json(raw_data)   
    
    lambda_l = data_df["data"]["lambda"]
    
    ##################################### MACHINES  ##########################################
    
            
    machines_df = pd.DataFrame(data_df["data"]["machines"])
    
    # Number of machines
    N_machines = len(machines_df)
            
    """
    machines_df contains all the information about machines. Because the MIP model uses a specific order
    of machines eg. from 0-3 are coil machines, 4-5 cutting and 6-8 bending we need to reorder the 
    df's rows accordinly. 
    """       
    
    # get the indexes from df given the machineType
    
    def machine_index(m_type):
        index = machines_df.index
        condition = machines_df["machineType"] == m_type
        indices = index[condition]  
        indices_list = indices.tolist() 
        
        return(indices_list)
        
    # sequence that is combatible with the MIP model eg. 0-3 coil machines, 4-5 cutting, 6-8 bending
    sequence = ["COIL", "CUT", "BEND"]
    
    #index = [get_index(i) for i in sequence]
    #index = list(itertools.chain(*index))
    
    index = list(itertools.chain(*[machine_index(i) for i in sequence]))
    
    # reindex the machines starting with coil, cut, bending and then reset the indexes from 0-8
    machines_df = machines_df.reindex(index).reset_index(drop=True)
    
    
    # make a new column with the setup time in minutes
    machines_df['setupTime_min'] = [round(time_to_min(t),2) for t in machines_df.loc[:,'setupTime']]
    
    
    # make a dictionary to map every stage with its machines, keys = sequence and values as below
    values = [list(machines_df.iloc[0:4,0]),list(machines_df.iloc[4:6,0]),list(machines_df.iloc[6:9,0])]
     
    stage_machines = dict(zip(sequence, values))
    
    ##########################################################################################   
    
    
    
    
    
    ################################### JOBS #################################################
    
    # load the orders' data 
    orders_df = pd.DataFrame(data_df["data"]["orders"]) 
    
    # Number of orders
    N_orders = len(orders_df) 
    
    # save all the different order ID
    order_id = orders_df["id"]
    
    # map the order with its jobs
    orders_jobs = {}
    j=0             # a pointer for the order position
    for key in order_id:
        orders_jobs[key] = tuple(orders_df["jobs"][j][i]["jobId"] for i in range(len(orders_df["jobs"][j])))
        j += 1
    
    # type of jobs
    Type = tuple(orders_df["jobs"][j][i]["processStage"] for j in range(N_orders)
                for i in range(len(orders_df["jobs"][j])))
    
    
    # Number of jobs
    N_jobs = len(Type) #sum(len(orders_jobs[key] ) for key in orders_jobs.keys() )
    
    
    """
    Due to lack of data we do not have every possible machine combination for a job. So we need to
    check whether a job can be processed on every possible machine (e.g. a coil job can be assigned
    among the 4 coil machines?) if this is not the case then we need to add the pair job:machine
    in the ForbiddenTasksToMachines list.
    
    The format of this list consist of pairs job:machine (e.g. '0:2') namely job 0 cannot be 
    assigned on machine 2. So we need a mapping between jobs_id and an integer number saying
    that job '5201210118005935' is job '0' for the MIP implementation etc
    """
    
    # give an integer number for every job id
    job_id = {}
    key = 0
    for i in orders_jobs.keys():
        for j in range(len(orders_jobs[i])):
            job_id[key] = orders_jobs[i][j]
            key += 1
    
    """
    Now we have to check whether a job can be processed on every possible/capable machine or not.
    For example if job '0' is a coil job then can be processed among all the coil machines ('10-format',
    '27-format4', 'format16', 'pbe2-16a') or a subset of them. If the later is the case then we have
    to create a pair in the ForbiddenTasksToMachines list (job:machine_id) and give an arbitrary small number as
    processing time.
    
    So initialize all the processing times to 0 and update them only for the machines we have 
    processing data
    """
    
    ProcessingTimes = np.zeros(shape=(N_machines,N_jobs))
    
    
    """
    Every order has its own DueDate, so it's assummed that every job that constitutes a specific order
    has the same DueDate. So, we create a list "Due" which contains the due date for every job
    """   
    Due=[time_to_min(orders_df["dueDate"][order_id[order_id == i].index[0]]) 
            for i in order_id for j in range(len(orders_jobs[i]))]
  
    
    forbidden = []
    """
    i : order pointer
    j : job pointer
    k : machine pointer for every job j, so it iterates over the "jobs" list
    """
    
    
    for i in range(N_orders):
        # save the order id
        order = order_id[i]
        
        for j in range(len(orders_df["jobs"][i])): # for every job in this order
            
            # save the job_id and its index
            job = orders_jobs[order][j]
            job_idx = getKey(job_id,job)[0]
            
            """
            The first step is to check the job type (e.g. coil,cut,bend). Let's assume the job is a 
            coil one, however it holds for every type of job. For the 'coil' job we create the 
            'machine_exist' dct and we initialize it with keys: all possible machines that can process
            the job, values:False.
            
            Then we keep the machine name and index from the "jobs" list. If the machine name is
            contained in the machine_exist.keys() (always is contained), update the value of the
            specific machine as True (this means that the machine is actually included in the possible
            processing machine list).
            
            For those machines we have duration data, calculate the processing time in minutes and 
            update the ProcessingTimes list.
            
            Finally for the machines that we should have data but we do not have we create the 
            "missing_machines" list filling it with those machine_exist.keys() which their values are
            False (actualy this means we cannot use this machine for this job assignment). Iterate
            over the "missing_machines" list and fabricate the forbidden (job:machine) pairs.
            
            """
            if orders_df["jobs"][i][j]['processStage'] == 0: # if it's a COIL job
                machine_exist = {key:False for key in (stage_machines['COIL'])} # machines Should be exist
                for k in range(len(orders_df["jobs"][i][j]['processingTimes'])):
                                
                    machine_name = orders_df["jobs"][i][j]['processingTimes'][k]['machineId']
                    # get the machine index given the machine id e.g. '10-FORMAT2' == 0
                    machine_idx = machines_df['machineId'].loc[lambda x: x == machine_name].index[0]
                                    
                    if machine_name in machine_exist:
                        
                        machine_exist[machine_name] = True
                        
                        duration = orders_df["jobs"][i][j]['processingTimes'][k]['duration']
                                            
                        ProcessingTimes[machine_idx][job_idx] = round(time_to_min(duration),2)
                    
                missing_machines = getKey(machine_exist,False)
                    
                for m in missing_machines:
                    machine_idx = machines_df['machineId'].loc[lambda x: x == m].index[0]
                    forbidden.append((job_idx,machine_idx))
                        
            elif orders_df["jobs"][i][j]['processStage'] == 1: # if it's a CUT job
                machine_exist = {key:False for key in (stage_machines['CUT'])} # machines Should be exist
                for k in range(len(orders_df["jobs"][i][j]['processingTimes'])):
                                
                    machine_name = orders_df["jobs"][i][j]['processingTimes'][k]['machineId']
                    machine_idx = machines_df['machineId'].loc[lambda x: x == machine_name].index[0]
                                    
                    if machine_name in machine_exist:
                        
                        machine_exist[machine_name] = True
                        
                        duration = orders_df["jobs"][i][j]['processingTimes'][k]['duration']
                                            
                        ProcessingTimes[machine_idx][job_idx] = round(time_to_min(duration),2)
                    
                missing_machines = getKey(machine_exist,False)
                    
                for m in missing_machines:
                    machine_idx = machines_df['machineId'].loc[lambda x: x == m].index[0]
                    forbidden.append((job_idx,machine_idx))
            
            
            else : # if it's a BEND job
                machine_exist = {key:False for key in (stage_machines['CUT'] + stage_machines['BEND'])} # machines Should be exist
                for k in range(len(orders_df["jobs"][i][j]['processingTimes'])):
                                
                    machine_name = orders_df["jobs"][i][j]['processingTimes'][k]['machineId']
                    machine_idx = machines_df['machineId'].loc[lambda x: x == machine_name].index[0]
                                    
                    if machine_name in machine_exist:
                        
                        machine_exist[machine_name] = True
                        
                        duration = orders_df["jobs"][i][j]['processingTimes'][k]['duration']
                                            
                        ProcessingTimes[machine_idx][job_idx] = round(time_to_min(duration),2)
                    
                missing_machines = getKey(machine_exist,False)
                    
                for m in missing_machines:
                    machine_idx = machines_df['machineId'].loc[lambda x: x == m].index[0]
                    forbidden.append((job_idx,machine_idx))
                        
    ProcessingTimes = ProcessingTimes.tolist()          
    """
    This is another way to tackle the above machine check and update the ProcessingTimes list and 
    forbidden list respectively
    """
    #if orders_df["jobs"][i][j]['processStage'] == 2:
    #    possible_machines = [machine for machine in (stage_machines['CUT'] + stage_machines['BEND'])]
    #    actual_machines = [orders_df["jobs"][i][j]['processingTimes'][k]['machineId'] 
    #                        for k in range(len(orders_df["jobs"][i][j]['processingTimes']))]      
    #    
    #    for k in range(len(orders_df["jobs"][i][j]['processingTimes'])):
    #                    
    #        machine_name = orders_df["jobs"][i][j]['processingTimes'][k]['machineId']
    #        machine_idx = machines_df['machineId'].loc[lambda x: x == machine_name].index[0]
    #        job_idx = getKey(job_id,job)[0]
    #                        
    #        duration = orders_df["jobs"][i][j]['processingTimes'][k]['duration']
    #                                
    #        ProcessingTimes[machine_idx][job_idx] = round(time_to_min(duration),2)
    #        
    #        
    #    missing_machines = list(set(possible_machines) - set(actual_machines))
    #    for m in missing_machines:
    #        machine_idx = machines_df['machineId'].loc[lambda x: x == m].index[0]
    #        forbidden.append((job_idx,machine_idx))
            
    
    """
    We have now pre-processed the data and converted it to the correct format. The next step is to 
    create a json file which the MIP model can read to solve the instance
    """
    # These parameters are constant
    N_Stages = 3
    MachinesPerStage = "4,2,3"
    
    
    requirements = np.array(["Tasks","Machines","Stages","MachinesPerStage","ReleaseTimes",
                            "DueTimes","SetUpTimes","MachineStatus","Type","lambda","ForbiddenPaths",
                            "ForbiddenTasksToMachines","ProcessingTimes"])
    
      
    """
    Retrieve the required data and create a json file
    """
        
    data = {}
    data["Tasks"] = str(N_jobs)
    data["Machines"] = str(N_machines)
    data["Stages"] = str(N_Stages)
    data["MachinesPerStage"] = MachinesPerStage
        
    #ReleaseTimes
    data['ReleaseTimes'] = ",".join(str(0) for x in range(N_jobs))
       
    #DueTimes 
    data['DueTimes']=",".join(str(x) for x in Due)
    
    #SetupTimes
    data['SetUpTimes'] = ",".join(str(x) for x in machines_df.iloc[:,4])
        
    #MachineStatus
    data['MachineStatus'] = ",".join(str(x) for x in machines_df.loc[:,'status'])
        
    #Type
    data['Type'] = ",".join(str(x) for x in Type)
        
    #lambda
    data['lambda'] = lambda_l
        
    #forbidden paths
    data["ForbiddenPaths"] = []
        
    #forbidden assignments
    data["ForbiddenTasksToMachines"] = ["".join(str(x)).strip('()') for x in forbidden]
        
    #ProcessingTimes
    data['ProcessingTimes'] = ["".join(str(x)).strip('[]') for x in ProcessingTimes]
        
        
        
    json_data = json.dumps(data)
    
             
    """
    After the MIP execution we need a post-process procedure to be complatible with the output
    we agreed in our deliverable document. So we need to reverse the MIP format (e.g. machines:
    0-8, jobs: 0-N_jobs etc) to one (machines : '10-FORMAT2','27-FORMAT4', jobs: 
    '5201210118005935' etc). Hence we need the mapping objects we created in the Input_Preprocess
    procedure to work with.
    """
    mapping_obj = machines_df, order_id, orders_jobs, job_id, ProcessingTimes,Type
    
    return(json_data, mapping_obj)  

    
 
    
    
    
    
    
    
    
    
        